<%-- 
    Document   : index.jsp
    Created on : Nov 29, 2015, 7:51:53 PM
    Author     : Krasse Dudes von JavAcademy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>  
    <%

        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            code ="";
        }

        String uErgebnis = (String) request.getAttribute("uErgebnis");

    %>
    <body>
        <h1>JavAcademy!</h1>
        <form action="DoCompiler.do" method="POST">
            <textarea name="code" rows="10" cols="50"><%=code%></textarea>
            <input type="submit" value="Los" />
        </form>        
            
         Musterbeispiel :<br> public String yourName(){<br>return "myNameTest";<br>}
    <br> <br>Konsolenausgabe:  <%=code%>
        <br>
        Compiler Ergebnis: <%=uErgebnis%>
    </body>
</html>
