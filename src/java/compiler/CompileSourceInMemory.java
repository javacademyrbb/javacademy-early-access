//package compiler;
//
//import javax.tools.Diagnostic;
//import javax.tools.DiagnosticCollector;
//import javax.tools.JavaCompiler;
//import javax.tools.JavaCompiler.CompilationTask;
//import javax.tools.JavaFileObject;
//import javax.tools.SimpleJavaFileObject;
//import javax.tools.ToolProvider;
//import java.io.File;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.StringWriter;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.net.MalformedURLException;
//import java.net.URI;
//import java.net.URL;
//import java.net.URLClassLoader;
//import java.util.Arrays;
//
//
//public class CompileSourceInMemory {
//    
////public static String getResult(String eingabe, String methodenparameter, String methodenname, String klassenname) throws MalformedURLException{
// 
//  public static String getResult(String eingabe) throws MalformedURLException{
//    String ergebnis = "";
//    
//            String expType = "String";
//            String methodenparameter = "String";
//            String methodenname = "getName";
//            String klassenname = "TestBeispiel";
//    
//    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//    DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
//
//    StringWriter writer = new StringWriter();
//    PrintWriter out = new PrintWriter(writer);
//    out.println("public class TestBeispiel{");
//    out.println("public String getName(){");
//    // Inhalt der Text Area von index.jsp
//    out.println(eingabe);
//    out.println("}");
//    out.println("}");
//    out.close();
//    eingabe = writer.toString();
//    JavaFileObject file = new JavaSourceFromString("TestBeispiel", eingabe);
//
//    Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(file);
//    CompilationTask task = compiler.getTask(null, null, diagnostics, null, null, compilationUnits);
//
//    boolean success = task.call();
//    for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
////      System.out.println(diagnostic.getCode());
////      System.out.println(diagnostic.getKind());
////      System.out.println(diagnostic.getPosition());
////      System.out.println(diagnostic.getStartPosition());
////      System.out.println(diagnostic.getEndPosition());
////      System.out.println(diagnostic.getSource());
//      ergebnis +="\n"+diagnostic.getMessage(null);
//    }
//    System.out.println("Success: " + success);
//
//    if (success) {
//      try {
//
//          URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { new File("").toURI().toURL() });
//          Method test1 = Class.forName("TestBeispiel", true, classLoader).getDeclaredMethod("getName");
//          ergebnis = (String) test1.invoke(out, null);
//
//      } catch (ClassNotFoundException e) {
//        System.err.println("Class not found: " + e);
//      } catch (NoSuchMethodException e) {
//        System.err.println("No such method: " + e);
//      } catch (IllegalAccessException e) {
//        System.err.println("Illegal access: " + e);
//      } catch (InvocationTargetException e) {
//        System.err.println("Invocation target: " + e);
//      }
//    }
//   return ergebnis;
//  }
//}
//
//class JavaSourceFromString extends SimpleJavaFileObject {
//  final String code;
//
//  JavaSourceFromString(String name, String code) {
//    super(URI.create("string:///" + name.replace('.','/') + Kind.SOURCE.extension),Kind.SOURCE);
//    this.code = code;
//  }
//
//  @Override
//  public CharSequence getCharContent(boolean ignoreEncodingErrors) {
//    return code;
//  }
//}

package compiler;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;


public class CompileSourceInMemory {
    
    public static void main(String[] args) throws MalformedURLException {
        System.out.println(getResult("return \"Ausgabe: Konsolenausgabe\";")); 
    }
 
  public static String getResult(String eingabe) throws MalformedURLException{
    String ergebnis = "";
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();

     StringWriter writer = new StringWriter();
    PrintWriter out = new PrintWriter(writer);
    out.println("public class HelloWorld {");
    out.println("  public static void main(String args[]) {");
    out.println("   System.out.println(\"okay\"); ");    
    out.println("  }");
    
    out.println(eingabe);
//    out.println(eingabe);
//    out.println(" }");
   out.println("}");
//   
    out.close();
    eingabe = writer.toString();
    JavaFileObject file = new JavaSourceFromString("HelloWorld", eingabe);

    Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(file);
    CompilationTask task = compiler.getTask(null, null, diagnostics, null, null, compilationUnits);

    boolean success = task.call();
    for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
//      System.out.println(diagnostic.getCode());
//      System.out.println(diagnostic.getKind());
//      System.out.println(diagnostic.getPosition());
//      System.out.println(diagnostic.getStartPosition());
//      System.out.println(diagnostic.getEndPosition());
//      System.out.println(diagnostic.getSource());
      ergebnis +="\n"+diagnostic.getMessage(null);
    }
    System.out.println("Success: " + success);

    if (success) {
      try {

          URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { new File("").toURI().toURL() });
          Method test1 = Class.forName("HelloWorld", true, classLoader).getDeclaredMethod("yourName");
          ergebnis = (String) test1.invoke(out, null);

      } catch (ClassNotFoundException e) {
        System.err.println("Class not found: " + e);
      } catch (NoSuchMethodException e) {
        System.err.println("No such method: " + e);
      } catch (IllegalAccessException e) {
        System.err.println("Illegal access: " + e);
      } catch (InvocationTargetException e) {
        System.err.println("Invocation target: " + e);
      }
    }
   return ergebnis;
  }
}

class JavaSourceFromString extends SimpleJavaFileObject {
  final String code;

  JavaSourceFromString(String name, String code) {
    super(URI.create("string:///" + name.replace('.','/') + Kind.SOURCE.extension),Kind.SOURCE);
    this.code = code;
  }

  @Override
  public CharSequence getCharContent(boolean ignoreEncodingErrors) {
    return code;
  }
}